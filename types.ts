export type inProduct = {
  id?: number
  reference: string
  prix: number
}

export type Product = {
  nom: string | undefined;
  categorie: string | undefined;
  images: string;
  taille: string;
  couleur: string;
  description: string | undefined;
  id?: number | undefined;
  reference: string;
  prix: number;
}