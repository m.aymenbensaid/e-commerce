import puppeteer from 'puppeteer';
import config from '../config';

let browser: any = null;
let pages = null;
let page: any = null;
const url = 'https://www.gimel-tunisie.com';

export default async function gimel(productRef: string | null) {
  try {
    if (!productRef) { throw new Error('End scraping'); }

    if (!browser) {
      browser = await puppeteer.launch(config);
      await browser.newPage();
      pages = await browser.pages()
      page = pages[0];
    }

    if (!page) { throw new Error('No page') }

    await page.goto(url, { waitUntil: 'networkidle2' });
    const show_search = await page.waitForSelector('#show_search');
    show_search.evaluate((el: any) => el.click());

    await page.waitForSelector('#leo_search_query_top');
    await page.type('#leo_search_query_top', productRef);

    console.log('> Start scraping:', productRef);

    await page.evaluate(() => document.getElementById('leo_search_query_top')!.click());
    await page.waitForSelector('.ac_results.lps_results');

    await page.evaluate(() => {
      const listProducts: any = document.querySelector('.ac_results.lps_results ul');
      listProducts?.children[0].click();
      console.log(listProducts);
    });

    await page.waitForSelector('#thumb-gallery');
    await page.waitForSelector('.slick-track');

    const product = await page.evaluate(() => {
      const name = document.querySelector('h1.product-detail-name')?.textContent
      const currentPrice = document.querySelector('.current-price')!.textContent!.replace(/\s+|\n|dt/gi, '');
      const description = document.querySelector('.product-description')?.textContent!.replace(/\s+|\n/g, ' ').trim();
      const reference = document.querySelector('span[itemprop="sku"]')?.textContent;

      const images = [...document.querySelectorAll('.slick-track img')].map((el: any) => el.src);
      const tailles = [...document.querySelectorAll('#group_1 li')].map(el => el.textContent?.replace(/\s+|\n/gi, ''));
      const couleurs = [...document.querySelectorAll('#group_2 li')].map(el => el.textContent?.replace(/\s+|\n/gi, ''));

      return {
        name,
        price: currentPrice,
        description,
        reference,
        images: images.join("///"),
        tailles: tailles.join("///"),
        couleurs: couleurs.join("///")
      }
    });

    console.log('< End scraping:', productRef, '\n');

    return product
  } catch (error: any) {
    await browser.close();
    console.error(error.message);
    process.exit(1);
  }
}