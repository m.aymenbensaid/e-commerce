import puppeteer, { Browser, Page } from 'puppeteer';
import config from '../config';
import fs from 'fs/promises';
import { json2csv } from 'json-2-csv';
import readXlsxFile from 'read-excel-file/node';
import { inProduct, Product } from "../types";
import colors from '../colors';

const inFilePath = process.cwd() + '/_in/mayoral.xlsx';
const outFileJSONPath = process.cwd() + '/_out/mayoral.json';
const outFileCSVPath = process.cwd() + '/_out/mayoral.csv';

let browser: Browser;
let pages: Page[];
let page: Page;
const url = 'https://www.mayoral.com/fr/france';

export default class Mayoral {
  private static async scrape(inProduct: inProduct | null) {
    try {
      if (!inProduct) { throw new Error('End scraping') }

      if (!browser) {
        browser = await puppeteer.launch(config);
        await browser.newPage();
        pages = await browser.pages()
        page = pages[0];
      }

      if (!page) { throw new Error('No page') }
      if (inProduct.reference?.includes('/')) inProduct.reference = inProduct.reference.replace('/', '-');

      await page.goto(url, { waitUntil: 'networkidle2' });
      // await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36');
      // await page.setExtraHTTPHeaders({ 'referer': 'https://www.mayoral.com/fr/france' });

      await page.evaluate(() => {
        if (document.querySelector('.cookie-banner__ctas button')) {
          (document.querySelector('.cookie-banner__ctas button') as HTMLButtonElement).click();
        }
      });

      const searchButton = await page.waitForSelector('#searchButton');
      searchButton!.evaluate((el: any) => el.click());

      await page.waitForSelector('#autoSuggest_result');
      await page.type('#autoSuggest_result input', inProduct.reference);

      const viewAllResults = await page.waitForSelector('#viewAllResults');
      viewAllResults!.evaluate((el: any) => el.click());

      await page.waitForSelector('.navbar');

      if (await page.evaluate(() => document.getElementById('showFacet') === null ? 0 : 1) === 1) {
        console.log(colors.FgRed, '----> Product is not found: ', inProduct.reference, colors.Reset, '\n');
        return [inProduct]
      }

      console.log(colors.FgGreen, '>> Start scraping:', inProduct.reference, colors.Reset);

      await page.waitForSelector('#product-carousel');
      await page.waitForSelector('.carousel-inner');

      const products = await page.evaluate(() => {
        const descriptionEl = document.querySelector('#product-description .contenido');
        let description = descriptionEl && descriptionEl.textContent && descriptionEl.textContent.length > 10
          ? descriptionEl.textContent
          : document.querySelector('#product-description .Eti')?.textContent;

        const categorie = document.getElementById('WC_BreadCrumb_Link_2')?.textContent?.trim().split(' ')[0];
        const nom = document.getElementById('InfoproductName')?.textContent?.trim();

        const images = [...document.querySelectorAll('.carousel-inner img')].map((el: any) => el.dataset.src || el.src) as string[];
        const tailles = [...document.querySelectorAll('.dropdown-menu.js-size-list a')].map(v => v.textContent?.trim()) as string[];
        const couleur = document.querySelector('.colors-carousel__item.active.selected')?.textContent as string;

        return tailles.filter(v => v).map(t => {
          const taille = t.trim().replace(/\n|\t|\t\n/g, '').replace(/\D+/g, '');
          return {
            nom,
            categorie,
            images: images.join('///'),
            taille: /mois/gi.test(t) ? taille + ' mois' : taille + ' ans',
            couleur: couleur.replace(/\s+|\n|\t|\t\n/g, '').trim(),
            description: description?.replace(/\s+|\n|\t|\t\n/g, ' ').trim()
          }
        })
      });

      console.log(colors.FgGreen, '<< End scraping:', inProduct.reference, colors.Reset, '\n');

      return (products as Product[]).map(p => {
        const ref = inProduct.reference.replace('-', '/');
        const reference = /ans/gi.test(p.taille) ? ref : ref + '-' + p.taille.replace(/Mois|ans/gi, '')
        return { ...inProduct, ...p, reference: reference.trim() }
      });

    } catch (error: any) {
      if (browser) await browser.close();
      console.error(error.message);
      process.exit(1);
    }
  }

  static async getProducts() {
    const data = await readXlsxFile(inFilePath);
    const referenceIndex = data[0].indexOf('reference');
    const prixIndex = data[0].indexOf('prix');
    const idIndex = data[0].indexOf('id');

    const products = data.filter(v => v).slice(1).map(d => {
      return { id: d[idIndex], reference: d[referenceIndex], prix: +d[prixIndex] }
    }) as inProduct[];

    const uniqProducts: inProduct[] = [];
    products.forEach(p => {
      if (!uniqProducts.some(v => v.reference.includes(p.reference))) uniqProducts.push(p);
    });

    console.log('\n---------------------------------------------------');
    console.log('> Total of products = ', products.length);
    console.log('> Total of uniq products = ', uniqProducts.length);
    console.log('> Columns = ', data[0]);
    console.log('> Uniq Product = ', uniqProducts[0]);
    console.log('---------------------------------------------------\n\n');

    return uniqProducts
  }

  static async run() {
    const data = await this.getProducts();
    let outProducts: any = [];
    let counter = data.length;

    data.forEach((inProduct: inProduct, index) => {
      let timeoutId = setTimeout(async () => {
        const p = await this.scrape(inProduct);

        if (p) {
          outProducts = [...outProducts, ...p];
          fs.writeFile(outFileJSONPath, JSON.stringify(outProducts));
          clearTimeout(timeoutId);
        }

        counter--;
        console.log(colors.FgYellow, '> Remains = ', counter, colors.Reset, '\n');

        if (counter === 0) {
          this.scrape(null);
          const csv = await json2csv(outProducts);
          if (csv) {
            fs.writeFile(outFileCSVPath, csv);
            clearTimeout(timeoutId);
          }
        }
      }, 1000 * (index === 0 ? 0 : index * 20));
    });
  }
}